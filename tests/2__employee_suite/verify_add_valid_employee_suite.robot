*** Comments ***
1. Navigate to the url
#LoginPage.resource
 2. Enter Admin username
 3. Enter password
 4. Click Login

 #MainPage.resource
 5. Click on PIM Menu
 #PIMPage.resource
 6. Click on Add Employee

 #AddEmployeePage.resource
 7. Enter firstname
 8. Enter middlename
 9. Enter lastname
 10. Upload the employee image
 11. Click on save"

 PersonalDetailPage.resource
  1. Add Employee header should be displayed
 2. Employee First Name should be displayed in the text box

 Data:
 1. Username: Admin
 2. Password: admin123
 3. Firstname: John
 4. MiddleName: J
 5. Last Name: Wick
 6.Employee Id: 4545
 7.Upload image: "C:\Users\JiDi\Pictures\Screenshots\Screenshot (1).png"

 1. Add Employee header should be displayed
 2. Employee First Name should be displayed in the text box

*** Settings ***
Documentation   This suite file verifies valid users are able to login to the dashboard
...     and connected to test case TC_OH_02

Resource    ../../resource/base/CommonFunctionalities.resource
Resource    ../../resource/pages/LoginPage.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown   Close Browser

*** Test Cases ***
Verify Add Valid Employee Test
    Enter Username      Admin
    Enter Password    password=admin123
    Click Login
   #complete below code
#     5. Click on PIM
    Click Element    xpath=//span[text()='PIM']
# 6. Click on Add Employee
    Click Element    link=Add Employee
# 7. Enter firstname
    Input Text    name=firstName    john
# 8. Enter middlename
    Input Text    name=middleName    j
# 9. Enter lastname
    Input Text    name=lastName    wick
# 10. Upload the employee image
    Choose File    xpath=//input[@type='file']    ${EXECDIR}${/}test_data${/}images${/}emp_john.jpg
# 11. Click on save"
    Click Element    xpath=//button[normalize-space()='Save']

#1. Added Employee Profile header should be displayed
#    Element Text Should Be    xpath=//h6[contains(normalize-space(),'john')]    john wick

#    Wait Until Element Contains    xpath=//div[@class='orangehrm-edit-employee-name']    john wick
#    Element Text Should Be    xpath=//div[@class='orangehrm-edit-employee-name']    john wick


## 2. Employee First Name should be displayed in the text box
#    Wait Until Element Is Visible    link=Contact Details     timeout=20s
#    Element Attribute Value Should Be    name=firstName    value    john
    ${result}   Set Variable    FAIL

    WHILE    '${result}'=='FAIL'
         ${output}   Run Keyword And Ignore Error    Element Text Should Be    xpath=//div[@class='orangehrm-edit-employee-name']    john wick
         Log     ${output}
         IF    '${output}[0]' == 'PASS'
              ${result}   Set Variable  PASS
              BREAK
         END
    END

    Should Be Equal As Strings    ${result}    PASS     Assertion on Header profile name

