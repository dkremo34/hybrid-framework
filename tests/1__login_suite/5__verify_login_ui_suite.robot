*** Settings ***
*** Settings ***
Documentation   This suite file verifies valid users are able to login to the dashboard
...     and connected to test case TC_OH_02

Resource    ../../resource/base/CommonFunctionalities.resource
Resource    ../../resource/pages/LoginPage.resource

Suite Setup      Launch Browser And Navigate To Url
Suite Teardown   Close Browser

*** Test Cases ***
Validate LoginPage Title 
    Validate LoginPage Title    expected_title=OrangeHRM

Validate Username and Password Placeholder
    Validate Username Placeholder    expected_placeholder_value=Username
    Validate Password Placeholder    expected_placeholder_value=Password