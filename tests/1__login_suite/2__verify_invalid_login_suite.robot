*** Settings ***
Documentation   This suite file verifies invalid users are not allowed to login to the dashboard
...     and it is connected to test case TC_OH_03
...     Test Template Concepts

Resource    ../../resource/base/CommonFunctionalities.resource
Resource    ../../resource/pages/LoginPage.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown   Close Browser

Test Template   Verify Invalid Login Template

*** Test Cases ***
TC1
    saul     saul123     Invalid credentials
TC2
    ${EMPTY}      kim123      Required
TC3
    Jessy      ${EMPTY}      Required


*** Keywords ***
Verify Invalid Login Template
    [Arguments]     ${username}     ${password}     ${expected_error}
    Enter Username      ${username}
    Enter Password    ${password}
    Click Login
    IF    '${username}'=='${EMPTY}'
        Validate Username Error Message    ${expected_error}
    ELSE IF     '${password}'=='${EMPTY}'
        Validate Password Error Message    ${expected_error}
    ELSE
        Validate Invalid Error Message        ${expected_error}
    END
