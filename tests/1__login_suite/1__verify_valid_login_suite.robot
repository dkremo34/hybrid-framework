*** Settings ***
Documentation   This suite file verifies valid users are able to login to the dashboard
...     and connected to test case TC_OH_02

Resource    ../../resource/base/CommonFunctionalities.resource
Resource    ../../resource/pages/LoginPage.resource
Resource    ../../resource/pages/DashboardPage.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown   Close Browser

#Test Template       Verify Valdid Login Template

*** Test Cases ***
TC1 Verify Valid Login Test
    [Template]      Verify Valdid Login Template
    Admin   admin123    Dashboard

TC2 Verify Valid Login Test
    [Template]      Verify Valdid Login Template
    Admin   admin123    Dashboard

TC3
    [Setup]
    Log To Console    Completed Task
    [Teardown]

*** Keywords ***
Verify Valdid Login Template
    [Arguments]     ${username}     ${password}     ${expected_header}
    Enter Username      ${username}
    Enter Password    ${password}
    Click Login
    Validate Dashboard Page Header  ${expected_header}