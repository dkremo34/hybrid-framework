*** Settings ***
Documentation   This suite file verifies invalid users are not allowed to login to the dashboard
...     and it is connected to test case TC_OH_03
...     Test Template Concepts With Excel

Resource    ../../resource/base/CommonFunctionalities.resource

Library     DataDriver      file=../../test_data/verify_invalid_login.csv

Test Setup      Launch Browser And Navigate To Url
Test Teardown   Close Browser

Test Template   Verify Invalid Login Template

*** Test Cases ***
Verify Invalid Login ${testcase_number}

*** Keywords ***
Verify Invalid Login Template
    [Arguments]     ${username}     ${password}     ${expected_error}
    Input Text    name=username    ${username}
    Input Text    name=password    ${password}
    Click Element    xpath=//button[normalize-space()='Login']
    Element Should Contain    xpath=//p[contains(normalize-space(),'Invalid')]        ${expected_error}